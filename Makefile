init:
	docker-compose up -d
	docker-compose run --rm --no-deps php bash -c "\
		composer install \
		&& bin/console doctrine:database:create --if-not-exists \
		&& bin/console doctrine:migrations:migrate --no-interaction \
	"

ssh-php:
	docker-compose run php bash

docker-stop:
	docker-compose down --remove-orphans --volumes