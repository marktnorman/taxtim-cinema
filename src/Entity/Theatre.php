<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TheatreRepository")
 */
class Theatre
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Film", mappedBy="theatre")
     */
    private $films;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\cinema", inversedBy="theatres")
     * @ORM\JoinColumn(nullable=false)
     */
    private $cinema;

    public function __construct()
    {
        $this->films = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->Name;
    }

    public function setName(string $Name): self
    {
        $this->Name = $Name;

        return $this;
    }

    /**
     * @return Collection|Film[]
     */
    public function getFilms(): Collection
    {
        return $this->films;
    }

    public function addFilm(Film $film): self
    {
        if (!$this->films->contains($film)) {
            $this->films[] = $film;
            $film->setTheatre($this);
        }

        return $this;
    }

    public function removeFilm(Film $film): self
    {
        if ($this->films->contains($film)) {
            $this->films->removeElement($film);
            // set the owning side to null (unless already changed)
            if ($film->getTheatre() === $this) {
                $film->setTheatre(null);
            }
        }

        return $this;
    }

    public function getCinema(): ?cinema
    {
        return $this->cinema;
    }

    public function setCinema(?cinema $cinema): self
    {
        $this->cinema = $cinema;

        return $this;
    }
}
