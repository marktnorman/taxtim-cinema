<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CinemaRepository")
 */
class Cinema
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Theatre", mappedBy="cinema")
     */
    private $theatres;

    public function __construct()
    {
        $this->theatres = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->Name;
    }

    public function setName(string $Name): self
    {
        $this->Name = $Name;

        return $this;
    }

    /**
     * @return Collection|Theatre[]
     */
    public function getTheatres(): Collection
    {
        return $this->theatres;
    }

    public function addTheatre(Theatre $theatre): self
    {
        if (!$this->theatres->contains($theatre)) {
            $this->theatres[] = $theatre;
            $theatre->setCinema($this);
        }

        return $this;
    }

    public function removeTheatre(Theatre $theatre): self
    {
        if ($this->theatres->contains($theatre)) {
            $this->theatres->removeElement($theatre);
            // set the owning side to null (unless already changed)
            if ($theatre->getCinema() === $this) {
                $theatre->setCinema(null);
            }
        }

        return $this;
    }
}
