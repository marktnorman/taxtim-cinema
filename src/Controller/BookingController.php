<?php

namespace App\Controller;

use App\Entity\Cinema;
use App\Entity\Theatre;
use App\Entity\Film;
use App\Entity\Booking;
use App\Entity\User;
use App\Form\BookingFormType;
use App\Repository\CinemaRepository;
use App\Repository\TheatreRepository;
use App\Repository\FilmRepository;
use App\Repository\BookingRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @Route("/booking")
 */
class BookingController extends AbstractController
{
    /**
     * @Route("/theatre/{cinema_id}", name="booking_theatre", methods={"GET"})
     * @param $cinema_id
     *
     * @return Response
     */
    public function bookingTheatre($cinema_id): Response
    {
        // Lets grab the theatres from cinema ID
        $theatres = $this->getDoctrine()
            ->getRepository(Theatre::class)
            ->findBy(['cinema' => $cinema_id]);

        // Lets loop through and grab the films from theatres ID
        $films = [];
        foreach ($theatres as $theatre) {
            $films[$theatre->getId()][] = $this->getDoctrine()
                ->getRepository(Film::class)
                ->findBy(['theatre' => $theatre->getId()]);
        }

        return $this->render(
            'booking/theatre.html.twig',
            [
                'theatres' => $theatres,
                'films'    => $films,
            ]
        );
    }

    /**
     * @Route("/film/{theatre_id}", name="booking_film", methods={"GET"})
     * @param $theatre_id
     *
     * @return Response
     */
    public function bookingFilm($theatre_id): Response
    {
        $films = $this->getDoctrine()
            ->getRepository(Film::class)
            ->findBy(['theatre' => $theatre_id]);

        return $this->render(
            'booking/films.html.twig',
            [
                'films' => $films,
            ]
        );
    }

    /**
     * @Route("/tickets/{film_id}", name="booking_tickets", methods={"GET", "POST"})
     * @param Request           $request
     * @param                   $film_id
     * @param BookingRepository $repository
     *
     * @return Response
     */
    public function bookingTickets(
        Request $request,
        $film_id,
        BookingRepository $repository
    ): Response {
        // Creating a booking
        $booking = new Booking();
        $userId  = $this->get('security.token_storage')->getToken()->getUser();
        if ($userId instanceof User) {
            $booking->setUser($this->get('security.token_storage')->getToken()->getUser());
            $booking->setFilmId($film_id);
            $booking->setReferenceNumber(time() . rand(10 * 45, 100 * 98));

            $form = $this->createForm(BookingFormType::class, $booking);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                $booking->setTotalTickets($request->request->get('booking_form')['totalTickets']);

                $repository->persist($booking);

                // Lets update the available tickets
                $entityManager  = $this->getDoctrine()->getManager();
                $filmRepository = $entityManager->getRepository(Film::class)->find($film_id);

                $currentRemainingTickets = $filmRepository->getAvailableTickets();
                $filmRepository->setAvailableTickets(
                    (int) $currentRemainingTickets - (int) $request->request->get('booking_form')['totalTickets']
                );
                $entityManager->persist($filmRepository);
                $entityManager->flush();

                return $this->redirectToRoute(
                    'user_bookings',
                    [
                        'userId' => $this->get('security.token_storage')->getToken()->getUser()
                    ]
                );
            }
        }

        return $this->render(
            'booking/tickets.html.twig',
            [
                'bookingForm' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/user-bookings/", name="user_bookings", methods={"GET"})
     *
     * @return Response
     */
    public function userBookings(): Response
    {
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->find($this->get('security.token_storage')->getToken()->getUser());

        $userBookings = $this->getDoctrine()
            ->getRepository(Booking::class)
            ->findBy(['user' => $this->get('security.token_storage')->getToken()->getUser()]);

        $films = [];

        foreach ($userBookings as $booking) {
            $films[$booking->getId()][] = $this->getDoctrine()
                ->getRepository(Film::class)
                ->find($booking->getFilmId());
        }

        return $this->render(
            'booking/bookings.html.twig',
            [
                'bookings' => $userBookings,
                'username' => $user->getUsername(),
                'films'    => $films
            ]
        );
    }

    /**
     * @param Request $request
     * @param Booking $booking
     * @Route("/delete", name="booking_delete", methods={"POST"})
     *
     * @return Response
     */
    public function bookingDelete(Request $request, Booking $booking): Response
    {
        // TODO - Check if the show is one hour away, if its more, allow user to delete their booking
        if ($this->isCsrfTokenValid('delete' . $booking->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($booking);
            $entityManager->flush();
        }

        return $this->redirectToRoute('index');
    }
}
