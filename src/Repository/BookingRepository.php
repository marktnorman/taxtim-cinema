<?php

namespace App\Repository;

use App\Entity\Booking;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\ORMException;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;

class BookingRepository extends ServiceEntityRepository
{
    private $logger;

    /**
     * BookingRepository constructor.
     *
     * @param RegistryInterface $registry
     * @param LoggerInterface   $logger
     */
    public function __construct(RegistryInterface $registry, LoggerInterface $logger)
    {
        parent::__construct($registry, Booking::class);
        $this->logger = $logger;
    }

    /**
     * @param Booking $booking
     */
    public function persist(Booking $booking): void
    {
        try {
            $this->_em->persist($booking);
            $this->_em->flush();
        } catch (ORMException $e) {
            $this->logger->error(
                sprintf('Unable to persist Booking %s with error: %s', $booking, $e->getMessage()),
                ['exception' => $e]
            );
        }
    }
}
