<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190624004339 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema) : void
    {
        $this->addSql("INSERT INTO film (name, start_time, end_time, available_tickets, theatre_id) VALUES ('Alien vs Predator', '2019-06-20 19:00:00', '2019-06-20 21:00:00', '30', '1')");
        $this->addSql("INSERT INTO film (name, start_time, end_time, available_tickets, theatre_id) VALUES ('Hangover 1', '2019-06-20 19:30:00', '2019-06-20 21:00:00', '30', '1')");

        $this->addSql("INSERT INTO film (name, start_time, end_time, available_tickets, theatre_id) VALUES ('Hangover 2', '2019-06-20 19:30:00', '2019-06-20 21:00:00', '30', '2')");
        $this->addSql("INSERT INTO film (name, start_time, end_time, available_tickets, theatre_id) VALUES ('Hangover 3', '2019-06-20 19:45:00', '2019-06-20 21:15:00', '30', '2')");

        $this->addSql("INSERT INTO film (name, start_time, end_time, available_tickets, theatre_id) VALUES ('Girls night out 1', '2019-06-20 19:35:00', '2019-06-20 21:05:00', '30', '3')");
        $this->addSql("INSERT INTO film (name, start_time, end_time, available_tickets, theatre_id) VALUES ('Girls night out 2', '2019-06-20 20:35:00', '2019-06-20 22:05:00', '30', '3')");

        $this->addSql("INSERT INTO film (name, start_time, end_time, available_tickets, theatre_id) VALUES ('Grind 1', '2019-06-20 17:35:00', '2019-06-20 19:05:00', '30', '4')");
        $this->addSql("INSERT INTO film (name, start_time, end_time, available_tickets, theatre_id) VALUES ('Grind 2', '2019-06-20 20:30:00', '2019-06-20 22:00:00', '30', '4')");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
