<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190624004334 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema) : void
    {
        $this->addSql("INSERT INTO theatre (name, cinema_id) VALUES ('Theatre A', '1')");
        $this->addSql("INSERT INTO theatre (name, cinema_id) VALUES ('Theatre B', '1')");
        $this->addSql("INSERT INTO theatre (name, cinema_id) VALUES ('Theatre C', '2')");
        $this->addSql("INSERT INTO theatre (name, cinema_id) VALUES ('Theatre D', '2')");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
