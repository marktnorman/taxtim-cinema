##TaxTim cinema assessment

### Setup

This application requires docker to be running.

Docker container consist of the following -

`PHP` - With name taxtim-cinema_php_1

`MySQL` - With name taxtim-cinema_mysql_1

`NGINX` - With name taxtim-cinema_nginx_1

- cd into the project root

`cd ~/path-to-project/taxtim-cinema`

- Run the initialization command from that directory

`make init`

Please copy `.env.test` to `.env` and uncomment lines, ideally in the real world these details should not be in the repo.

The app will now be accessible at http://127.0.0.1:8080

### Extra

Clearing cache - 

`bin/console c:c`